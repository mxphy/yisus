﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yisus.Internals
{
    public class Client
    {
        public static bool IsShowingScreen { get; set; }
        public static bool IsRegisteringKeys { get; set; }
        public static string CurrentWindow { get; set; }
    }
}
