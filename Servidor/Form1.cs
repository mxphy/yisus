﻿using SuperSocket.SocketBase;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace ScreenServer
{
    public partial class Yisus : Form
    {
        Form3 input;
        Form4 input2;
        private static System.Timers.Timer aTimer;
        public Yisus()
        {
            aTimer = new System.Timers.Timer(3000);
            aTimer.Elapsed += OnTick;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;

            InitializeComponent();

            CheckForIllegalCrossThreadCalls = false;
        }

        private void OnTick(object sender, ElapsedEventArgs e)
        {
            label3.Text = $"Angels connected: {Server.BotsConnected}";
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            input = new Form3();
            input.Show();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            input2 = new Form4();
            input2.Show();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
