﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScreenServer
{
    public class Server
    {
        public static bool IsListening { get; set; } = false;
        public static bool IsListeningKeys { get; set; } = false;
        public static string KeyRecollect { get; set; } = "";
        public static bool IsShowingScreen { get; set; }
        public static int BotsConnected { get; set; } = 1;
    }
}
